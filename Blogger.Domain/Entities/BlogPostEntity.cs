﻿namespace Blogger.Domain.Entities
{
    public class BlogPostEntity: Entity
    {
        

        public string Subject { get; set; }

        public string Body { get; set; }

        public int BloggerId { get; set; }

        public BloggerEntity Blogger { get; set; }
    }
}
