﻿using System.Collections.Generic;

namespace Blogger.Domain.Entities
{
    public class BloggerEntity : Entity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IList<BlogPostEntity> BlogPosts { get; set; }

        public int? BloggerId { get; set; }
    }

}
