﻿using System;
namespace Blogger.Domain.Services
{
    public interface IDistanceComputationService
    {
        int Compute(int fromBloggerId, int toBLoggerId);
    }
}
