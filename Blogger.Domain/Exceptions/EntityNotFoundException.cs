﻿using System;
namespace Blogger.Domain.Exceptions
{
    public class EntityNotFoundException: Exception
    {
        public EntityNotFoundException(string message):base(message)
        {

        }
    }
}
