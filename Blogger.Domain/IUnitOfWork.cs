﻿namespace Blogger.Domain
{
    public interface IUnitOfWork
    {
        void Save();
    }
}
