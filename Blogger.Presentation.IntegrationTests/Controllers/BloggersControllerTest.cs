﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Blogger.Presentation.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]
namespace Blogger.Presentation.IntegrationTests.Controllers
{
    public class BloggersControllerTest
    {
        private HttpClient _client;

        public BloggersControllerTest()
        {
            var builder = new WebHostBuilder()
              .UseStartup<Startup>();

            TestServer testServer = new TestServer(builder);
            _client = testServer.CreateClient();
        }

        private async void _WhenCallIndex_ShouldGetBloggerList(int expectedCount)
        {
            var response = await _client.GetAsync("/api/Bloggers");

            var content = await response.Content.ReadAsStringAsync();
            var models = JsonConvert.DeserializeObject<IList<BloggerModelForGet>>(content);

            Assert.True(response.IsSuccessStatusCode);
            Assert.True(models.Count == expectedCount);
        }

        private async void _WhenCallGet_ShouldGetBlogger(int id, string firstName, string lastName)
        {
            var response = await _client.GetAsync($"/api/Bloggers/{id}");

            var content = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<BloggerModelForGet>(content);

            Assert.True(response.IsSuccessStatusCode);
            Assert.True(model.Id == id);
            Assert.True(model.FirstName == firstName);
            Assert.True(model.LastName == lastName);
        }

        [Fact]
        public async void WhenCallIndex_ShouldGetBloggerList()
        {
            _WhenCallIndex_ShouldGetBloggerList(3);
        }

        [Fact]
        public async void WhenCallGet_ShouldGetModel()
        {
            _WhenCallGet_ShouldGetBlogger(1, "David", "Saurel");
        }

        [Fact]
        public async void WhenCallGet_ShouldReturn404()
        {
            var response = await _client.GetAsync("/api/bloggers/10");

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.NotFound);
        }

        [Fact]
        public async void WhenCallDelete_ShouldReturn200()
        {
            var response = await _client.DeleteAsync("/api/bloggers/1");

            Assert.True(response.IsSuccessStatusCode);

            _WhenCallIndex_ShouldGetBloggerList(2);
        }

        [Fact]
        public async void WhenCallDelete_ShouldReturn404()
        {
            var response = await _client.DeleteAsync("/api/bloggers/10");

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.NotFound);
        }

        [Fact]
        public async void WhenCallPost_ShoulReturn200()
        {
            var model = new BloggerModelForPost { FirstName = "Test", LastName = "Test" };
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/bloggers", content);

            Assert.True(response.IsSuccessStatusCode);

            _WhenCallIndex_ShouldGetBloggerList(4);
        }

        [Fact]
        public async void WhenCallPost_ShoulReturn400()
        {
            var model = new BloggerModelForPost();
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/Bloggers", content);

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.BadRequest);
        }

        [Fact]
        public async void WhenCallPut_ShouldReturn200()
        {
            var model = new BloggerModelForPut() { Id = 1, FirstName = "Test", LastName = "Test" };
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/bloggers", content);

            Assert.True(response.IsSuccessStatusCode);

            _WhenCallGet_ShouldGetBlogger(1, "Test", "Test");
        }

        [Fact]
        public async void WhenCallPut_ShouldReturn404()
        {
            var model = new BloggerModelForPut() { Id = 10, FirstName = "Test", LastName="Test" };
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/bloggers", content);

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.NotFound);
        }

        [Fact]
        public async void WhenCallPatch_ShouldReturn200()
        {
            var model = new BloggerModelForPatch() { Id = 1, FirstName = "Test"};
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PatchAsync("/api/Bloggers", content);

            Assert.True(response.IsSuccessStatusCode);

            _WhenCallGet_ShouldGetBlogger(1, "Test", "Saurel");
        }

        [Fact]
        public async void WhenCallPatch_ShouldReturn404()
        {
            var model = new BloggerModelForPatch() { Id = 10, FirstName = "Test" };
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PatchAsync("/api/bloggers", content);

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.NotFound);
        }

        [Fact]
        public async void WhenCallDistance_ShouldReturnCorrectDistance()
        {
            var response = await _client.GetAsync("/api/bloggers/distance/3/1");

            Assert.True(response.IsSuccessStatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.True(content == "0");

            response = await _client.GetAsync("/api/bloggers/distance/1/2");

            Assert.True(response.IsSuccessStatusCode);

            content = await response.Content.ReadAsStringAsync();
            Assert.True(content == "1");

            response = await _client.GetAsync("/api/bloggers/distance/1/3");

            Assert.True(response.IsSuccessStatusCode);

            content = await response.Content.ReadAsStringAsync();
            Assert.True(content == "2");
        }

        [Fact]
        public async void WhenCallDistance_ShouldReturn404()
        {
            var response = await _client.GetAsync("/api/bloggers/distance/10/1");

            Assert.False(response.IsSuccessStatusCode);
            Assert.True(response.StatusCode == HttpStatusCode.NotFound);
        }
    }
}
