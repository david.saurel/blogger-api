﻿using System;
using System.Collections.Generic;
using System.Net;
using AutoMapper;
using Blogger.Domain;
using Blogger.Domain.Entities;
using Blogger.Domain.Exceptions;
using Blogger.Domain.Services;
using Blogger.Presentation.Models;
using Microsoft.AspNetCore.Mvc;

namespace Blogger.Presentation.Mapper.Controllers
{
    [Route("api/[controller]")]
    public class BloggersController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRepository<BloggerEntity> _repository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDistanceComputationService _distanceComputationService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public BloggersController(
            IRepository<BloggerEntity> repository,
            IMapper mapper,
            IDistanceComputationService distanceComputationService
            )
        {
            _repository = repository;
            _mapper = mapper;
            _distanceComputationService = distanceComputationService;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Index()
        {
            var entities = _repository.All();
            var models = _mapper.Map<IEnumerable<BloggerEntity>, IEnumerable<BloggerModelForGet>>(entities);
            return new OkObjectResult(models);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var entity = _repository.Get(id);
            var model = _mapper.Map<BloggerEntity, BloggerModelForGet>(entity);
            return new OkObjectResult(model);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] BloggerModelForPost model)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var entity = _mapper.Map<BloggerModelForPost, BloggerEntity>(model);
            _repository.Add(entity);
            return new OkResult();
        }

        // POST api/<controller>
        [HttpPatch]
        public IActionResult Patch([FromBody] BloggerModelForPatch model)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var entity = _mapper.Map<BloggerModelForPatch, BloggerEntity>(model);
            _repository.PartialUpdate(entity);
            return new OkResult();
        }

        // POST api/<controller>
        [HttpPut]
        public IActionResult Put([FromBody] BloggerModelForPut model)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var entity = _mapper.Map<BloggerModelForPut, BloggerEntity>(model);
            _repository.Update(entity);
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _repository.Remove(id);
            return new OkResult();
        }

        [HttpGet("distance/{from}/{to}")]
        public IActionResult Distance(int from, int to)
        {
            //Check blogger exists
            _repository.Get(to);
            _repository.Get(from);

            return new OkObjectResult(_distanceComputationService.Compute(from, to));
        }
    }
}
