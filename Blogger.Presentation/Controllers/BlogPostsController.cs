﻿using AutoMapper;
using Blogger.Domain;
using Blogger.Domain.Entities;
using Blogger.Presentation.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Blogger.Presentation.Mapper.Controllers
{
    public class BlogPostsController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRepository<BlogPostEntity> _blogPostRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRepository<BloggerEntity> _bloggerRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public BlogPostsController(
            IRepository<BlogPostEntity> blogPostRepository,
            IRepository<BloggerEntity> bloggerRepository,
            IMapper mapper
            )
        {
            _blogPostRepository = blogPostRepository;
            _mapper = mapper;
            _bloggerRepository = bloggerRepository;
        }

        // GET api/<controller>/5
        [HttpGet("api/[controller]/{id}")]
        public IActionResult Index(int id)
        {
            var entity = _blogPostRepository.Get(id);
            var model = _mapper.Map<BlogPostEntity, BlogPostModelForGet>(entity);
            return new OkObjectResult(model);
        }

        // GET api/<controller>/5
        [HttpGet("api/{id}/[controller]")]
        public IActionResult GetBlogPost(int id)
        {
            var entities = _blogPostRepository.Find(entity => entity.BloggerId == id);
            var models = _mapper.Map<IEnumerable<BlogPostEntity>, IEnumerable<BlogPostModelForGet>>(entities);
            return new OkObjectResult(models);
        }

        [HttpPost("api/{id}/[controller]/batch")]
        public IActionResult Batch([FromBody] IList<BlogPostModelForPost> models, int id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            _bloggerRepository.Get(id);
            var entities = _mapper.Map<IEnumerable<BlogPostModelForPost>, IEnumerable<BlogPostEntity>>(models);
            foreach(var entity in entities)
            {
                entity.BloggerId = id;
            }
            _blogPostRepository.AddRange(entities.ToList());
            return new OkResult();
        }

        // POST api/<controller>
        [HttpPatch]
        public IActionResult Patch([FromBody] BlogPostModelForPatch model)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var entity = _mapper.Map<BlogPostModelForPatch, BlogPostEntity>(model);
            _blogPostRepository.PartialUpdate(entity);
            return new OkResult();
        }

        // POST api/<controller>
        [HttpPost("api/{id}/[controller]")]
        public IActionResult Post([FromBody] BlogPostModelForPost model, int id)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            _bloggerRepository.Get(id);
            var entity = _mapper.Map<BlogPostModelForPost, BlogPostEntity>(model);
            entity.BloggerId = id;
            _blogPostRepository.Add(entity);
            return new OkResult();
        }

        [HttpPut("api/[controller]")]
        public IActionResult Put([FromBody] BlogPostModelForPut model)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var entity = _mapper.Map<BlogPostModelForPut, BlogPostEntity>(model);
            _blogPostRepository.Update(entity);
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("api/[controller]/{id}")]
        public IActionResult Delete(int id)
        {
            _blogPostRepository.Remove(id);
            return new OkResult();
        }
    }
}
