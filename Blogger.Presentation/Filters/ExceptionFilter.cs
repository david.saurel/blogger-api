﻿using System.Net;
using Blogger.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Blogger.Presentation.Filters
{
    public class ExceptionFilter: IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if(context.Exception is EntityNotFoundException)
            {
                context.Result = new NotFoundResult();
            }
            context.Result = new ContentResult { StatusCode = (int)HttpStatusCode.InternalServerError, Content = context.Exception.Message };
        }
    }
}
