﻿using System;
using AutoMapper;
using Blogger.Application.Services;
using Blogger.Domain;
using Blogger.Domain.Entities;
using Blogger.Domain.Services;
using Blogger.Infrastructure.EntityFramework;
using Blogger.Presentation.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Blogger.Presentation
{
    public class Startup
    {
        private const string databaseName = "Blogger";
        private const string ApiVersion = "v1";
        private const string ApiName = "Blogger WebApi Rest";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BloggerDbContext>(options => options.UseInMemoryDatabase(databaseName: databaseName));
            services.AddScoped<IDistanceComputationService, DistanceComputationService>();
            services.AddScoped<IRepository<BloggerEntity>, BloggerRepository>();
            services.AddScoped<IRepository<BlogPostEntity>, BlogPostRepository>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(ApiVersion, new Info { Title = ApiName, Version = ApiVersion });
            });
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.ConfigureCustomExceptionMiddleware();
            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{ApiVersion}/swagger.json", $"{ApiName} {ApiVersion}");
            });
        }
    }
}
