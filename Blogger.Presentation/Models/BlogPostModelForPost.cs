﻿using System.ComponentModel.DataAnnotations;

namespace Blogger.Presentation.Models
{
    public class BlogPostModelForPost
    {
        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
