﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blogger.Presentation.Models
{
    public class BloggerModelForPost
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}
