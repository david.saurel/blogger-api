﻿using System.ComponentModel.DataAnnotations;

namespace Blogger.Presentation.Models
{
    public class BlogPostModelForPatch
    {
        public string Subject { get; set; }

        public string Body { get; set; }

        [Required]
        public int Id { get; set; }

        public int BloggerId { get; set; }
    }
}
