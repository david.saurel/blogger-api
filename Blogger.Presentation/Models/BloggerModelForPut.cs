﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blogger.Presentation.Models
{
    public class BloggerModelForPut: BloggerModelForPost
    {
        [Required]
        public int Id { get; set; }
    }
}
