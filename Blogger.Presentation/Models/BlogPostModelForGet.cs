﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blogger.Presentation.Models
{
    public class BlogPostModelForGet
    {
        public string Subject { get; set; }

        public string Body { get; set; }

        public int BloggerId { get; set; }
    }
}
