﻿using System.ComponentModel.DataAnnotations;

namespace Blogger.Presentation.Models
{
    public class BloggerModelForPatch
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required]
        public int Id { get; set; }
    }
}
