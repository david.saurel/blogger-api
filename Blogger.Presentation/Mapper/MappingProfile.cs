﻿using System;
using AutoMapper;
using Blogger.Domain.Entities;
using Blogger.Presentation.Models;

namespace Blogger.Presentation.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BloggerEntity, BloggerModelForGet>();
            CreateMap<BloggerModelForPatch, BloggerEntity>();
            CreateMap<BloggerModelForPost, BloggerEntity>();
            CreateMap<BloggerModelForPut, BloggerEntity>();

            CreateMap<BlogPostEntity, BlogPostModelForGet>();
            CreateMap<BlogPostModelForPost, BlogPostEntity>();
            CreateMap<BlogPostModelForPut, BlogPostEntity>();
            CreateMap<BlogPostModelForPatch, BlogPostModelForGet>();
        }
    }
}
