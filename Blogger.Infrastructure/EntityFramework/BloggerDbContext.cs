﻿using Blogger.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Blogger.Infrastructure.EntityFramework
{
    public class BloggerDbContext : DbContext
    {
        public BloggerDbContext(DbContextOptions<BloggerDbContext> options)
        : base(options)
        {
        }

        public DbSet<BloggerEntity> Bloggers { get; set; }

        public DbSet<BlogPostEntity> BlogPosts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BloggerEntity>(entity =>
            {    
                entity.Property(e => e.Id).IsRequired();
                entity.Property(e => e.BloggerId);
                entity.Property(e => e.FirstName).IsRequired();
                entity.Property(e => e.LastName).IsRequired();
                entity
                .HasMany<BlogPostEntity>()
                .WithOne(c => c.Blogger)
                .HasForeignKey(c => c.BloggerId);
            });

            modelBuilder.Entity<BlogPostEntity>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
                entity.Property(e => e.Subject).IsRequired();
                entity.Property(e => e.Body).IsRequired();
                entity.HasKey(e => e.Id);
                entity
                .HasOne(p => p.Blogger)
                .WithMany(b => b.BlogPosts)
                .HasForeignKey(p => p.BloggerId);
            });


            modelBuilder.Entity<BloggerEntity>().HasData(
                new BloggerEntity { Id = SequenceGenerator.Instance.NextId, FirstName = "David", LastName = "Saurel", BloggerId = 2},
                new BloggerEntity { Id = SequenceGenerator.Instance.NextId, FirstName = "Pierre-Yves", LastName = "Dezaunay", BloggerId = 3 },
                new BloggerEntity { Id = SequenceGenerator.Instance.NextId, FirstName = "Nicolas", LastName = "Beraud" });

            modelBuilder.Entity<BlogPostEntity>().HasData(
                new BlogPostEntity { Id = SequenceGenerator.Instance.NextId, BloggerId = 1, Body = "Microsoft intègre le side projet WAS Blazor à la roadmap de .net Core 3.0 et en profite pour le renommer Razor Component.", Subject = "Razor Component" },
                new BlogPostEntity { Id = SequenceGenerator.Instance.NextId, BloggerId = 2, Body = "Après 2 ans de financement en fond propre, la société bordelaise souhaite passer à la vitesse supérieur avec une première lever de fond.", Subject = "1er lever de fond" },
                new BlogPostEntity { Id = SequenceGenerator.Instance.NextId, BloggerId = 3, Body = "Betclic déménage et s'installe dans le quartir du bassin à flot à Bordeaux.", Subject = "Un autre ténor parisien spécialisé dans le pari sportif débarque à Bordeaux" }

                );
        }
    }
}
