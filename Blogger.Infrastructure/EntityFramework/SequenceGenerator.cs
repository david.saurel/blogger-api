﻿using System;
namespace Blogger.Infrastructure.EntityFramework
{
    /// <summary>
    /// https://github.com/aspnet/EntityFrameworkCore/issues/12371
    /// </summary>
    public class SequenceGenerator
    {
        private static SequenceGenerator _instance;

        private int _currentId;

        private SequenceGenerator()
        {
        }

        public static SequenceGenerator Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new SequenceGenerator();
                }
                return _instance;
            }
        }

        public int NextId
        {
            get
            {
                return ++_currentId;
            }
        }
    }
}
