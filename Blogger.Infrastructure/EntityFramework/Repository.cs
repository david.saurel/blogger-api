﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blogger.Domain;
using Blogger.Domain.Entities;
using Blogger.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Blogger.Infrastructure.EntityFramework
{
    public class Repository<TEntity>: IRepository<TEntity> where TEntity: Entity
    {
        protected readonly BloggerDbContext _bloggerDbContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bloggerDbContext"></param>
        public Repository(BloggerDbContext bloggerDbContext)
        {
            _bloggerDbContext = bloggerDbContext;
            _bloggerDbContext.Database.EnsureCreated();
        }

        public void Add(TEntity entity)
        {
            entity.Id = SequenceGenerator.Instance.NextId;

            _bloggerDbContext.Set<TEntity>().Add(entity);
            _bloggerDbContext.SaveChanges();
        }

        public void AddRange(IList<TEntity> entities)
        {
            foreach(var entity in entities)
            {
                entity.Id = SequenceGenerator.Instance.NextId;
                _bloggerDbContext.Set<TEntity>().Add(entity);
            }
            _bloggerDbContext.SaveChanges();
        }

        public IEnumerable<TEntity> All()
        {
            return _bloggerDbContext.Set<TEntity>().ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> Find(Func<TEntity,bool> predicate)
        {
            return _bloggerDbContext.Set<TEntity>().Where(predicate).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            var entity = _bloggerDbContext.Set<TEntity>().FirstOrDefault(e => e.Id == id);
            if(entity == null)
            {
                throw new EntityNotFoundException($"{typeof(TEntity).Name}({id})");
            }
            _bloggerDbContext.Remove<TEntity>(entity);
            _bloggerDbContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public TEntity Get(int id)
        {
            var entity = _bloggerDbContext.Set<TEntity>().FirstOrDefault(e => e.Id == id);
            if (entity == null)
            {
                throw new EntityNotFoundException($"{typeof(TEntity).Name}({id})");
            }
            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public void Update(TEntity entity)
        {
            try
            {
                _bloggerDbContext.Attach(entity);
                _bloggerDbContext.Entry(entity).State = EntityState.Modified;
                _bloggerDbContext.SaveChanges();
            }
            catch(DbUpdateConcurrencyException)
            {
                throw new EntityNotFoundException($"{typeof(TEntity).Name}({entity.Id})");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public virtual void PartialUpdate(TEntity entity){
            
        }
    }
}
