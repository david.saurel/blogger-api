﻿using Blogger.Domain.Entities;

namespace Blogger.Infrastructure.EntityFramework
{
    public class BlogPostRepository : Repository<BlogPostEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bloggerDbContext"></param>
        public BlogPostRepository(BloggerDbContext bloggerDbContext) : base(bloggerDbContext)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public override void PartialUpdate(BlogPostEntity entity)
        {
            var persisted = Get(entity.Id);
            bool save = false;
            if (!string.IsNullOrWhiteSpace(entity.Subject))
            {
                save = true;
                persisted.Subject = entity.Subject;
            }

            if (!string.IsNullOrWhiteSpace(entity.Body))
            {
                save = true;
                persisted.Body = entity.Body;
            }

            if (entity.BloggerId > 0)
            {
                save = true;
                persisted.BloggerId = entity.BloggerId;
            }

            if (save)
            {
                _bloggerDbContext.SaveChanges();
            }
        }
    }
}
