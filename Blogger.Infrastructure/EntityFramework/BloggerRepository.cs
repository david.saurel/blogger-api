﻿using System;
using Blogger.Domain.Entities;

namespace Blogger.Infrastructure.EntityFramework
{
    /// <summary>
    /// 
    /// </summary>
    public class BloggerRepository : Repository<BloggerEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bloggerDbContext"></param>
        public BloggerRepository(BloggerDbContext bloggerDbContext) : base(bloggerDbContext)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public override void PartialUpdate(BloggerEntity entity)
        {
            var persisted = Get(entity.Id);
            bool save = false;
            if (!string.IsNullOrWhiteSpace(entity.FirstName))
            {
                save = true;
                persisted.FirstName = entity.FirstName;
            }

            if (!string.IsNullOrWhiteSpace(entity.LastName))
            {
                save = true;
                persisted.LastName = entity.LastName;
            }

            if (save)
            {
                _bloggerDbContext.SaveChanges();
            }
        }
    }
}
