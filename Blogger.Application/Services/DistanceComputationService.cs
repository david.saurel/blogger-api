﻿using Blogger.Domain;
using Blogger.Domain.Entities;
using Blogger.Domain.Exceptions;
using Blogger.Domain.Services;

namespace Blogger.Application.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class DistanceComputationService : IDistanceComputationService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRepository<BloggerEntity> _repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        public DistanceComputationService(IRepository<BloggerEntity> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromBloggerId"></param>
        /// <param name="toBLoggerId"></param>
        /// <returns></returns>
        public int Compute(int fromBloggerId, int toBLoggerId)
        {
            _repository.Get(fromBloggerId);
            _repository.Get(toBLoggerId);

            return _Compute(fromBloggerId, toBLoggerId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromBloggerId"></param>
        /// <param name="toBLoggerId"></param>
        /// <param name=""></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private int _Compute(int fromBloggerId, int toBLoggerId, int distance = 0)
        {
            var fromEntity = _repository.Get(fromBloggerId);
            if (fromEntity.BloggerId.HasValue)
            {
                if (fromEntity.BloggerId.Value == toBLoggerId)
                {
                    return ++distance;
                }
                return _Compute(fromEntity.BloggerId.Value, toBLoggerId, ++distance);
            }

            return 0;
        }
    }
}
